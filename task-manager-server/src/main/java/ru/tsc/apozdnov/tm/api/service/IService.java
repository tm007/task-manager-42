package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.repository.IRepository;
import ru.tsc.apozdnov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}